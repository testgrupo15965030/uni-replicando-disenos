<?php
if ($_FILES['pdfFile']['error'] == UPLOAD_ERR_OK && is_uploaded_file($_FILES['pdfFile']['tmp_name'])) {
    $uploadDir = 'uploads/';
    $nameFile = uniqid().".pdf";
    $uploadFile = $uploadDir . $nameFile;

    if (move_uploaded_file($_FILES['pdfFile']['tmp_name'], $uploadFile)) {
        echo $nameFile;
    } else {
        echo "Error al guardar el archivo.";
    }
} else {
    echo "Error al subir el archivo.";
}
?>