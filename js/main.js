const botonMenu = document.getElementById('menu-btn');
const mainRigth = document.querySelector('.main-rigth');
const mainLeft = document.querySelector('.main-left');
const btnOption = document.getElementById('main-left__option-one');
const optionOne=document.querySelector('.main-left__item-one');

let mesesPeriodo1 = [1, 2, 3, 4, 5, 6];
let mesesPeriodo2 = [7, 8, 9, 10, 11, 12];

let fechaActual = new Date();
let añoActual = fechaActual.getFullYear();
let mesActual = fechaActual.getMonth() + 1; // Nota: Los meses comienzan en 0 (enero es 0)

let periodoActual = mesesPeriodo1.includes(mesActual) ? "10" : "20";

let arrayDomicilio = [];
let arrayFormacion = [];

function generateUniqueId() {
    return 'pdf_' + Date.now() + '_' + Math.floor(Math.random() * 1000);
}

botonMenu.addEventListener('click', function(){
    mainRigth.classList.toggle('active');
    mainLeft.classList.toggle('active');
});

btnOption.addEventListener('click',function(){
    optionOne.classList.toggle('active');
});

document.getElementById("informacion-docente").addEventListener("click", function(){
    informacionDocente();
});

//HORARIO DE CLASES
  
function horarioClases(){
    const http = new XMLHttpRequest();
    const url = "http://127.0.0.1:5502/horario-clase.html";
    http.onreadystatechange = function(){
        if(this.readyState==4 && this.status==200){

            //console.log(this.responseText);
            document.getElementById("main-rigth__container").innerHTML = this.responseText;

            //ARMAR HORARIO
            let tbody = document.getElementById("tbody-horario");

            let semana = ["LU", "MA", "MI", "JU", "VI", "SA", "DO"];
            
            fetch('../json/horario-final.json')
            .then(data => data.json())
            .then(horarios => {

                let hora_inicio = 7;
                let hora_fin = 22;

                tbody.innerHTML = ``;

                let tr = ``;

                let dias_tr = {
                        LU: 0,
                        MA: 0,
                        MI: 0,
                        JU: 0,
                        VI: 0,
                        SA: 0,
                        DO: 0
                    }

                for (let num = hora_inicio; num <= hora_fin; num++) {

                    let hora = `${ num.toString().padStart(2, '0') } - ${ (num + 1).toString().padStart(2, '0') }`;

                    tr = `<tr><td>${hora}</td>`;

                    let horarioHoraActual = [];

                    for (let horario of horarios) {
                        let rangoHora = horario.hora.split("-");
                        if (rangoHora[0] == num) {
                            horarioHoraActual.push(horario);
                        }
                    }

                    for (let dia of semana) {
                        let infoDia = null;
                        for (diaActual of horarioHoraActual) {
                            if (dia == diaActual.nDia) {
                                infoDia = diaActual;
                            }
                        }
                        if (infoDia) {
                            let horaInicioFin = infoDia.hora.split("-");
                            let diferencia = horaInicioFin[1] - horaInicioFin[0];
                            dias_tr[dia] -= (diferencia - 1);
                            tr += `<td rowspan="${diferencia}" class="pruebatr">
                                <i class="bi bi-person-fill"></i>
                                (${infoDia.numAlum})${infoDia.codCurso} /[VIRTUAL](${infoDia.teopra})
                            </td>`;
                        } else {
                            if (dias_tr[dia] == 0) {
                                tr += `<td></td>`;
                            } else {
                                dias_tr[dia] += 1;
                            }
                        }
                    }

                    tr += `</tr>`;

                    tbody.innerHTML += tr;

                }

            });
        }
    }
    http.open("GET", url);
    http.send();
}

document.getElementById("horario-clase").addEventListener("click", function(){
    horarioClases();
});
/*--------------------------------------------------------------------------------------------------*/   

//INFORMACION DOCENTE
function informacionDocente(){
const http = new XMLHttpRequest();
const url = "http://127.0.0.1:5502/informacion-docente.html";
http.onreadystatechange = function(){

    if(this.readyState==4 && this.status==200){

        let rutaDni = "uploads/";
        let archivoDni = "";
        // console.log(this.responseText);
        document.getElementById("main-rigth__container").innerHTML = this.responseText;

        //CONSUMIR DATOS CON FETCH API EN LA TABLA DOMICILIO
        let infoDomicilio= document.getElementById("infoDomicilio");
        let datosFormacion= document.getElementById("datosFormacion");

        //datosDomicilio();
        //dataFormacion();
        cargarHorarioDocente();

        let btnGuardarFormacion = document.getElementById("btn-guardar-formacion");

        let btnGuardarDomicilio = document.getElementById("btn-guardar-domicilio");
        //datos de subir archivo

        let verDni = document.getElementById("ver-dni");

        let btnEliminarDni = document.getElementById("eliminar-dni");

        let divVerDni = document.getElementById("div-dni");

        btnEliminarDni.addEventListener('click', function(e) {
            archivoDni = "";
        });

        verDni.addEventListener('click', function(e) {
            if (archivoDni) {
                let ruta = rutaDni + archivoDni;
                divVerDni.innerHTML = `<div >
                                            <embed src="${ruta}" type="application/pdf" width="100%" height="630">
                                        </div>
                `
            } else {
                divVerDni.innerHTML = `No hay archivo que mostrar`;
            }
        });

        btnGuardarFormacion.addEventListener('click', function(e) {
            e.preventDefault();

            let ordenFormacion = arrayFormacion.length;
            let formacionProfesional = document.getElementById("formacionProfesional");
            let paisFormacion = document.getElementById("paisFormacion");
            let universidadFormacion = document.getElementById("universidadFormacion");
            let especialidadFormacion = document.getElementById("especialidadFormacion");
            let fechaInicioFormacion = document.getElementById("fechaInicioFormacion");
            let fechaFinFormacion = document.getElementById("fechaFinFormacion");
            let gradoObtenidoFormacion = document.getElementById("gradoObtenidoFormacion");
            let documentoFormacion = "";
            let observacionFormacion = document.getElementById("observacionFormacion");

            let formacion = {
                orden: ordenFormacion + 1,
                formacion_profesional: formacionProfesional.value,
                pais: paisFormacion.value,
                universidad: universidadFormacion.value,
                especialidad: especialidadFormacion.value,
                fecha_inicio: fechaInicioFormacion.value,
                fecha_final: fechaFinFormacion.value,
                grado_obtenido: gradoObtenidoFormacion.value,
                documento: documentoFormacion,
                observacion: observacionFormacion.value
            }

            arrayFormacion.push(formacion);

            tablaFormacion(arrayFormacion);

            modalFormacion.classList.remove("modal--show");

            universidadFormacion.value = "";
            especialidadFormacion.value = "";
            gradoObtenidoFormacion.value = "";
            observacionFormacion.value = "";

        });

        function enviarArchivo() {

            let formulario = document.getElementById("uploadForm");

            let formData = new FormData(formulario);

            fetch('subir-archivo.php', {
                method: 'POST',
                body: formData
            })
            .then(response => response.text())
            .then(function(data) {
                console.log(data);
                archivoDni = data;
                //document.getElementById('message').innerHTML = data;
            })
            .catch(function(error) {
                console.error('Error:', error);
            });
        }

        btnGuardarDomicilio.addEventListener('click', function(e) {
            e.preventDefault();

            let ordenDomicilio = arrayDomicilio.length;
            let paisDomicilio = document.getElementById("paisDomicilio");
            let departamentoDomicilio = document.getElementById("departamentoDomicilio");
            let provinciaDomicilio = document.getElementById("provinciaDomicilio");
            let distritoDomicilio = document.getElementById("distritoDomicilio");
            let tipoViaDomicilio = document.getElementById("tipoViaDomicilio");
            let nombreViaDomicilio = document.getElementById("nombreViaDomicilio");
            let tipoInmuebleDomicilio = document.getElementById("tipoInmuebleDomicilio");
            let numeroInmuebleDomicilio = document.getElementById("numeroInmuebleDomicilio");
            let tipoZonaDomicilio = document.getElementById("tipoZonaDomicilio");
            let nombreZonaDomicilio = document.getElementById("nombreZonaDomicilio");

            let domicilio = {
                orden: ordenDomicilio + 1,
                pais: paisDomicilio.value,
                direccion:"",
                departamento: departamentoDomicilio.value,
                privincia: provinciaDomicilio.value,
                distrito: distritoDomicilio.value,
                tipo_via: tipoViaDomicilio.value,
                nombre_via: nombreViaDomicilio.value,
                tipo: tipoInmuebleDomicilio.value,
                numero_inmueble: numeroInmuebleDomicilio.value,
                tipo_zona: tipoZonaDomicilio.value,
                nombre_zona: nombreZonaDomicilio.value
            }

            arrayDomicilio.push(domicilio);

            tablaDomicilio(arrayDomicilio);

            modalDomicilio.classList.remove("modal--show-domicilio");

            nombreViaDomicilio.value = "";
            numeroInmuebleDomicilio.value = "";
            nombreZonaDomicilio.value = "";
            
        });

        //Subir archivo PDF
        let icon_archivo=document.getElementById('icon-archivo');
        let formulario = document.getElementById('uploadForm');
        let inputFile = document.getElementById('pdfFile');

        icon_archivo.addEventListener('click',()=> {
            inputFile.click();
        });

        inputFile.addEventListener('change', function() {
            if (this.value) {
                enviarArchivo();
            }
        });

        

        

        //TABS
        let tabs=Array.prototype.slice.apply(document.querySelectorAll('.tabs__item'));
        let panels = Array.prototype.slice.apply(document.querySelectorAll('.panels__item'));
        document.getElementById('tabs').addEventListener('click',  e => {
            if(e.target.classList.contains('tabs__item')){
                let i = tabs.indexOf(e.target);
    
                tabs.map(tab => tab.classList.remove('active'));
                tabs[i].classList.add('active');
    
                panels.map(panel => panel.classList.remove('active'));
                panels[i].classList.add('active');
            }
        });

            let botonFormacion = document.getElementById("botonFormacion");
            let modalFormacion = document.getElementById("modalFormacionDocente");
            let closeModal = document.querySelector(".modal__close");

            let botonDomicilio = document.getElementById("botonDomicilio");
            let modalDomicilio = document.getElementById("modalAgregarDomicilio");
            let closeDomicilio = document.getElementById("closeDomicilio");


            let modalPdf = document.getElementById("modalVerPDF");
            let close_PDF = document.getElementById("close_PDF");

            //FORMULARIO AGREGAR FORMACIONN

            botonFormacion.addEventListener('click',(e)=>{
                e.preventDefault();
                modalFormacion.classList.add("modal--show");
            });

            closeModal.addEventListener('click',(e)=>{
                e.preventDefault();
                modalFormacion.classList.remove("modal--show");
            });
            
            //-----------------------------------------------
            //FORMULARIO AGREGAR DOMICILIO

            botonDomicilio.addEventListener('click',(e)=>{
                e.preventDefault();
                modalDomicilio.classList.add("modal--show-domicilio");
            });
            closeDomicilio.addEventListener('click',(e)=>{
                e.preventDefault();
                modalDomicilio.classList.remove("modal--show-domicilio");
            });
            //Formulario vista PDF DNI
            verDni.addEventListener('click',(e)=>{
                e.preventDefault();
                modalPdf.classList.add("modal--show-PDF");
            });
            close_PDF.addEventListener('click',(e)=>{
                e.preventDefault();
                modalPdf.classList.remove("modal--show-PDF");
            });
        
        }
    }
    http.open("GET", url);
    http.send();
}

    function datosDomicilio(){
        fetch('../json/domicilio.json')
            .then(res => res.json())
            .then(datos => {
                tablaDomicilio(datos);
                console.log(datos);
            });
    }
    function dataFormacion(){
        fetch('../json/datos-formacion.json')
            .then(res => res.json())
            .then(datos => {
                tablaFormacion(datos);
                console.log(datos);
            });
    }

    
/*-------------------------------------------------------*/

    
/*-------------------------------------------------------*/
        function tablaDomicilio(datos){
            infoDomicilio.innerHTML=``;
            for(let valor of datos){
                infoDomicilio.innerHTML+=`
                <tr>
                    <td>${valor.orden}</td>
                    <td>${valor.pais}</td>
                    <td>${valor.direccion}</td>
                    <td>${valor.departamento}</td>
                    <td>${valor.privincia}</td>
                    <td>${valor.distrito}</td>
                    <td>${valor.tipo_via}</td>
                    <td>${valor.nombre_via}</td>
                    <td>${valor.tipo}</td>
                    <td>${valor.numero_inmueble}</td>
                    <td>${valor.tipo_zona}</td>
                    <td>${valor.nombre_zona}</td>
                </tr>
                `
                }
            }
            function tablaFormacion(datos){
                datosFormacion.innerHTML=``;
                for(let valor of datos){
                    datosFormacion.innerHTML+=`
                    <tr>
                        <td>${valor.orden}</td>
                        <td>${valor.formacion_profesional}</td>
                        <td>${valor.pais}</td>
                        <td>${valor.universidad}</td>
                        <td>${valor.especialidad}</td>
                        <td>${valor.fecha_inicio}</td>
                        <td>${valor.fecha_final}</td>
                        <td>${valor.grado_obtenido}</td>
                        <td>${valor.documento}</td>
                        <td>${valor.observacion}</td>
                        <td>
                            <button class="button  button--blue"><i class="bi bi-pencil-fill"></i></button>
                            <button class="button  button--blue"><i class="bi bi-dash"></i></button>
                        </td>
                    </tr>
                    `
                }
            }

            function cargarHorarioDocente() {

                let horarioDocente = document.getElementById("tbody-horario-docente");

                fetch('../json/horario-docente.json')
                .then(data => data.json())
                .then(horarios => {
            
                    horarioDocente.innerHTML = ``;
            
                    for (let horario of horarios) {
            
                        let tr = ``;
            
                        for (let detalle of horario.detalles) {
            
                            tr +=  `
                                <tr>
                            `
            
                            let vuelta_actual = horario.detalles.indexOf(detalle);
            
                            if (vuelta_actual == 0) {
                                console.log(detalle.aula)
                                tr += `
                                    <td rowspan="${horario.detalles.length}">${horario.codigo}</td>
                                    <td rowspan="${horario.detalles.length}">${horario.nombre_curso}</td>
                                    <td>${detalle.aula}</td>
                                    <td>${detalle.tpl}</td>
                                    <td>${detalle.horario}</td>
                                    <td>${detalle.ultima_revision}</td>
                                `;
                            } else {
                                tr += `
                                    <td>${detalle.aula}</td>
                                    <td>${detalle.tpl}</td>
                                    <td>${detalle.horario}</td>
                                    <td>${detalle.ultima_revision}</td>
                                `;
                            }
            
                            tr += `
                                </tr>
                            `;
                        }
        
                        horarioDocente.innerHTML += tr;
        
                    }
            
                });
            
            }
    
/*--------------------------------------------------------------------------------------------------*/   
    //MI ASISTENCIA
    function miAsistencia(){
        const http = new XMLHttpRequest();
        const url = "http://127.0.0.1:5502/asistencia.html";
        http.onreadystatechange = function(){
            if(this.readyState==4 && this.status==200){
                // console.log(this.responseText);
                document.getElementById("main-rigth__container").innerHTML = this.responseText;
                

                let selectSemestre=document.getElementById("select-semestre");
                let validateYearAndPeriod = false;

                fetch("../json/select-semestre.json")
                .then(res => res.json())
                .then(semestres => {
                    
                    for (let semestre of semestres) {
                        var optionSemestre = document.createElement('option');
                        optionSemestre.value = semestre.semestre;
                        optionSemestre.text = semestre.semestre;
                        let infoSemestre = semestre.semestre.split("-");

                        let año = infoSemestre[0];
                        let periodo = infoSemestre[1];

                        if (periodoActual == periodo && añoActual == año) {
                            validateYearAndPeriod = true;
                            optionSemestre.selected = true;
                        }
                        selectSemestre.add(optionSemestre);
                    }

                    if (selectSemestre.value) {
                        datosAsistencia(selectSemestre.value);
                    }

                });

                
                selectSemestre.addEventListener("change",()=>{

                    if (selectSemestre.value) {
                        datosAsistencia(selectSemestre.value);
                        let infoSemestre = selectSemestre.value.split("-");
                        let año = infoSemestre[0];
                        let periodo = infoSemestre[1];
                        if (periodoActual == periodo && añoActual == año) {
                            validateYearAndPeriod = true;
                        }

                    } else {
                        infoAsistencia.innerHTML = ``;
                    }

                });
                
                let infoAsistencia = document.getElementById("asistencia-mont");
                let dataAsistencia="";

                function datosAsistencia(archivo){
                    fetch(`../json/asistencia-${archivo}.json`)
                        .then(res => res.json())
                        .then(meses => {

                            cargarMesesAsistencia(meses);

                        })
                        .then(meses => {

                            if (validateYearAndPeriod) {
                                let divMesActual = document.getElementById("mes-"+mesActual);
                                divMesActual.click();
                                validateYearAndPeriod = false;
                            }
        
                            
                            
                        });
                }

                function cargarMesesAsistencia(meses) {
                    
                    dataAsistencia = ``;

                    for (let mes of meses) {
                        dataAsistencia +=`
                        <div class="btn btn--grey color-white mont" id="mes-${mes.numero_mes}">${mes.nombre}</div>
                        <div id="" class="none table-mont">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Dia</th>
                                        <th >Curso</th>
                                        <th>Horario</th>
                                        <th >Marcacion</th>
                                        <th>Est</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody-mes-${mes.numero_mes}">
                                </tbody>
                            </table>

                            <div class="attendance attendance-center">
                                <span><strong>Resumen de Asistencia del semestre</strong></span><br>
                                <span><strong>P</strong>:43  <strong>T</strong>:0<strong>  F</strong>:0</span>
                            </div>
                        </div> `;

                    }

                    infoAsistencia.innerHTML = dataAsistencia;

                    let mesElements  = document.querySelectorAll(".mont");
                    let table_mesElements = document.querySelectorAll(".table-mont");
                    mesElements.forEach((mesElements, index) => {
                        mesElements.addEventListener('click', function() {
                            table_mesElements[index].classList.toggle("none");
                    
                            if (mesElements.classList.contains("btn--blue")) {
                                mesElements.classList.remove("btn--blue");
                                mesElements.classList.add("btn--grey");
                            } else {
                                mesElements.classList.remove("btn--grey");
                                mesElements.classList.add("btn--blue");
                                
                                //Consulta cada vez que se hace click a los meses
                                cargarTablaPorMes(this.id);
                            }
                        });
                    });

                }

                function cargarTablaPorMes(id) {
                    fetch("../json/tabla-asistencia.json")
                    .then(res => res.json())
                    .then(elementos => {
                        let fila = ``;
                        let tbodyElement = document.getElementById("tbody-" + id);
                        for (let elemento of elementos) {
                            fila += `<tr>
                                        <td>${elemento.dia}</td>
                                        <td>${elemento.curso}</td>
                                        <td>${elemento.horario}</td>
                                        <td>${elemento.marcacion}</td>
                                        <td><button class="btn btn--green">P</button></td>
                                    </tr>`
                        }
                        tbodyElement.innerHTML = fila;
                    });
                }

                function tablaAsistencia(datos){
                    for(let valor of datos){
                        dataAsistencia+=`
                        <div class="btn btn--grey color-white mont" id="">${valor.nombre}</div>
                        <div id="" class="none table-mont">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Dia</th>
                                        <th >Curso</th>
                                        <th>Horario</th>
                                        <th >Marcacion</th>
                                        <th>Est</th>
                                    </tr>
                                </thead>
                                <tbody"> `
                                    for(let dato of valor.datos){
                                        dataAsistencia+=`
                                                <tr>
                                                    <td>${dato.dia}</td>
                                                    <td>${dato.curso}</td>
                                                    <td>${dato.horario}</td>
                                                    <td>${dato.marcacion}</td>
                                                    <td><button class="btn btn--green">P</button></td>
                                                </tr>
                                    `
                                    }
                                    dataAsistencia+=`
                                </tbody>
                                
                            </table>
                            <div class="attendance attendance-center">
                                    <span><strong>Resumen de Asistencia del semestre</strong></span><br>
                                    <span><strong>P</strong>:43  <strong>T</strong>:0<strong>  F</strong>:0</span>
                                </div>
                            </div>    
                            `
                                  
 

                        
                    }
                    infoAsistencia.innerHTML = dataAsistencia;
                    
                    let mesElements  = document.querySelectorAll(".mont");
                    let table_mesElements = document.querySelectorAll(".table-mont");
                    mesElements.forEach((mesElements, index) => {
                        mesElements.addEventListener('click', function() {
                            table_mesElements[index].classList.toggle("none");
                    
                            if (mesElements.classList.contains("btn--blue")) {
                                mesElements.classList.remove("btn--blue");
                                mesElements.classList.add("btn--grey");
                            } else {
                                mesElements.classList.remove("btn--grey");
                                mesElements.classList.add("btn--blue");
                            }
                        });
                    });

                }
            }

           
        }
        http.open("GET", url);
        http.send();
    }

    document.getElementById("mi-asistencia").addEventListener("click", function(){
        miAsistencia();
    })
  
    


   
    
    
    
    


    
