(function(d){
    let tabs=Array.prototype.slice.apply(d.querySelectorAll('.tabs__item'));
    let panels = Array.prototype.slice.apply(d.querySelectorAll('.panels__item'));
    d.getElementById('tabs').addEventListener('click',  e => {
        if(e.target.classList.contains('tabs__item')){
           let i = tabs.indexOf(e.target);

           tabs.map(tab => tab.classList.remove('active'));
           tabs[i].classList.add('active');

           panels.map(panel => panel.classList.remove('active'));
           panels[i].classList.add('active');
        }
    });

    //FORMULARIO AGREGAR FORMACION

    let botonFormacion = document.getElementById("botonFormacion");
    let modalFormacion = document.getElementById("modalFormacionDocente");
    let closeModal = document.querySelector(".modal__close");

    let botonDomicilio = document.getElementById("botonDomicilio");
    let modalDomicilio = document.getElementById("modalAgregarDomicilio");
    let closeDomicilio = document.getElementById("closeDomicilio");


    //FORMULARIO AGREGAR FORMACIONN

    botonFormacion.addEventListener('click',(e)=>{
        e.preventDefault();
        modalFormacion.classList.add("modal--show");
    });

    closeModal.addEventListener('click',(e)=>{
        e.preventDefault();
        modalFormacion.classList.remove("modal--show");
    });
    
    //-----------------------------------------------
      //FORMULARIO AGREGAR DOMICILIO

      botonDomicilio.addEventListener('click',(e)=>{
        e.preventDefault();
        modalDomicilio.classList.add("modal--show-domicilio");
    });
    closeDomicilio.addEventListener('click',(e)=>{
        e.preventDefault();
        modalDomicilio.classList.remove("modal--show-domicilio");
    });
 
//CONSUMIR DATOS CON FETCH API EN LA TABLA DOMICILIO

    let infoDomicilio= document.getElementById("infoDomicilio");

    function datosDomicilio(){
        fetch('json/domicilio.json')
            .then(res => res.json())
            .then(datos => {
                tabla(datos);
            });

    }
    function tabla(datos){
        for(let valor of datos){
            infoDomicilio.innerHTML+=`
            <tr>
                <td>${valor.orden}</td>
                <td>${valor.pais}</td>
                <td>${valor.direccion}</td>
                <td>${valor.departamento}</td>
                <td>${valor.privincia}</td>
                <td>${valor.distrito}</td>
                <td>${valor.tipo_via}</td>
                <td>${valor.nombre_via}</td>
                <td>${valor.tipo}</td>
                <td>${valor.numero_inmueble}</td>
            </tr>
            `
        }
    }

    window.addEventListener("load", function() {
        datosDomicilio();
    });
//---------------------------------------------------------------------------

})(document);

 // {
    //     "orden":"BEF01-C",
    //     "pais":"ETICA Y FILOSOFIA POLITICA",
    //     "direccion":"A2-340",
    //     "departamento":"T",
    //     "privincia":"VI 13-14",
    //     "distrito":"vie., 3 de mar. del 2023 9:11",
    //     "tipo-via":"",
    //     "nombre-via":"",
    //     "tipo":"Mz",
    //     "numero-inmueble":"F"

    // }